/*! (c) jTorm and other contributors | https://jtorm.com/license */

const
  config = JSON.parse(process.env.RETHINKDB),
  { dbModel } = require('@jtorm/db'),
  { errorHandlerPoolModel } = require('@jtorm/error-handler'),
  r = require('rethinkdb'),
  { rethinkdbModel } = require('./models/rethinkdb.model'),
  { rethinkdbErrorHandlerModel } = require('./models/rethinkdb/rethinkdb-error-handler.model')
;

dbModel.instance = rethinkdbModel;

errorHandlerPoolModel.emerg.push(rethinkdbErrorHandlerModel);

module.exports = {
  init: async () => {
    console.log('Connecting RethinkDB');

    rethinkdbModel.connection = await r.connect(config);

    console.log('Connected RethinkDB');
  },

  rethinkdbModel
};
