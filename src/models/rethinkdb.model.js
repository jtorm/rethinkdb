/*! (c) jTorm and other contributors | https://jtorm.com/license */

const
  { errorHandlerModel, EntityDoesNotExistsError } = require('@jtorm/error-handler'),
  r = require('rethinkdb'),
  { ReqlDriverCompileError } = require('rethinkdb/errors'),
  { ValidationError } = require('joi'),
  rethinkdbModel = {
    connection: null,

    read: (dbName, tableName, filterObj, selectArray) => {
      if (filterObj && filterObj['@id'])
        delete filterObj['@id']
      ;
  
      return new Promise(async (res, rej) => {
        try {
          const result = await r
            .db(dbName)
            .table(tableName)
            .filter(filterObj)
            .run(rethinkrethinkdbModel.connection)
          ;

          result.toArray((err, result) => {
            if (err) {
              errorHandlerModel.error(err);
              return rej(err);
            }

            if (!result.length)
              return rej(new EntityDoesNotExistsError(filterObj))
            ;

            if (selectArray) {
              const resultFiltered = [];

              for (let k in result) {
                const rowObj = {};

                for (let k2 in selectArray) {
                  if (result[k][selectArray[k2]])
                    rowObj[selectArray[k2]] = result[k][selectArray[k2]]
                  ;
                }

                resultFiltered.push(rowObj);
              }

              return res(resultFiltered);
            }

            return res(result);
          });
        } catch (e) {
          errorHandlerModel.error(e);
          return rej(e);
        }
      });
    },

    create: (dbName, tableName, recordObj) => {
      return new Promise(async (res, rej) => {
        try {
          const result = await r
            .db(dbName)
            .table(tableName)
            .insert([recordObj])
            .run(rethinkdbModel.connection)
          ;

          return res(result);
        } catch (e) {
          if (e instanceof ReqlDriverCompileError)
            return rej(new ValidationError(e))
          ;

          errorHandlerModel.error(e);
          return rej(e);
        }
      });
    },

    update: (dbName, tableName, selectObj, updateObj) => {
      return new Promise(async (res, rej) => {
        try {
          const result = await r
            .db(dbName)
            .table(tableName)
            .filter(selectObj)
            .update(updateObj)
            .run(rethinkdbModel.connection)
          ;

          return res(result);
        } catch (e) {
          errorHandlerModel.error(e);
          return rej(e);
        }
      });
    },

    delete: (dbName, tableName, selectObj) => {
      return new Promise(async (res, rej) => {
        try {
          const result = await r
            .db(dbName)
            .table(tableName)
            .filter(selectObj)
            .delete()
            .run(rethinkdbModel.connection)
          ;

          return res(result);
        } catch (e) {
          errorHandlerModel.error(e);
          return rej(e);
        }
      });
    },

    createDb: (dbName) => {
      return new Promise(async (res, rej) => {
        try {
          const exists = await r
            .dbList()
            .contains(dbName)
            .run(rethinkdbModel.connection)
          ;

          if (exists)
            return res(true)
          ;

          const result = await r
            .dbCreate(dbName)
            .run(rethinkdbModel.connection)
          ;

          return res(result);
        } catch (e) {
          errorHandlerModel.error(e);
          return rej(e);
        }
      });
    },

    createTable: (dbName, tableName) => {
      return new Promise(async (res, rej) => {
        try {
          const tableList = await r
            .db(dbName)
            .tableList()
            .run(rethinkdbModel.connection)
          ;

          if (tableList.indexOf(tableName) !== -1)
            return res(true)
          ;

          const result = await r
            .db(dbName)
            .tableCreate(tableName)
            .run(rethinkdbModel.connection)
          ;

          return res(result);
        } catch (e) {
          errorHandlerModel.error(e);
          return rej(e);
        }
      });
    },

    dropTable: (dbName, tableName) => {
      return new Promise(async (res, rej) => {
        try {
          const tableList = await r.db(dbName).tableList().run(rethinkdbModel.connection);

          if (tableList.indexOf(tableName) === -1)
            return res(true)
          ;

          const result = await r
            .db(dbName)
            .tableDrop(tableName)
            .run(rethinkdbModel.connection)
          ;

          return res(result);
        } catch (e) {
          errorHandlerModel.error(e);
          return rej(e);
        }
      });
    }
  }
;

module.exports = {
  rethinkdbModel
};
