/*! (c) jTorm and other contributors | https://jtorm.com/license */

const
  { rethinkdbModel } = require('./../rethinkdb.model')
;

const rethinkdbErrorHandlerModel = {
  execute: () => {
    rethinkdbModel.connection.close(
      function(err) {
        if (err)
          console.error(err)
        ;
      }
    )
  }
};

module.exports = {
  rethinkdbErrorHandlerModel
};
