# jTorm RethinkDB

## Install

```js
npm install @jtorm/rethinkdb
```

## Config
.env variable `RETHINKDB` with options from the connect method: https://rethinkdb.com/api/javascript/connect/
